const allItems = document.querySelectorAll('.js-navItem');

const allPics = document.querySelectorAll('.js-navPic');
let currentImage = null;

for(i=0; i<allItems.length; i++){
   allItems[i].addEventListener('mouseover', e => {
   		const imgId = parseInt(e.target.getAttribute('data-target'));
     allPics[imgId].classList.remove('hidden');
     currentImage = allPics[imgId];
     });
    
		allItems[i].addEventListener('mouseout', e =>{
    	const imgId = parseInt(e.target.getAttribute('data-target'));
      allPics[imgId].classList.add('hidden');
    })
}

const container = document.querySelector('.js-containerPic');
container.addEventListener('mousemove', e => {
  if (currentImage) {
    currentImage.style.transform = "translate("+e.pageX - 150+"px,"+e.pageY - 150+"px);";
    // currentImage.style.left = (e.pageX - 150)+"px";
    // currentImage.setAttribute("style", "transform: translateY("+(e.pageY - 150)+"px) translateX("+(e.pageX - 150)+"px); display: block;");
  }
})