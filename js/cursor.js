const areaCursor = document.querySelector('.js-cursor');
const cursor = document.querySelector('.cursor');

areaCursor.addEventListener('mousemove', e => {
    // cursor.setAttribute("style", "top: "+(e.pageY - 10)+"px; left: "+(e.pageX - 10)+"px; display: block;");
    cursor.setAttribute("style", "transform: translateY("+(e.pageY - 10)+"px) translateX("+(e.pageX - 10)+"px); display: block;");
})
areaCursor.addEventListener('mouseout', e =>{
    cursor.setAttribute("style", "display: none;");
})